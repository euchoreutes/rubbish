package com.maipiande.rubbish.controller;

import com.github.pagehelper.PageInfo;
import com.maipiande.rubbish.RubbishResult;
import com.maipiande.rubbish.entity.Article;
import com.maipiande.rubbish.param.article.ArticleListParam;
import com.maipiande.rubbish.param.article.ArticleSaveParam;
import com.maipiande.rubbish.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    private ArticleService articleService;


    /**
     * 文章列表
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model){
        PageInfo<Article> list = this.articleService.list(new ArticleListParam());
        model.addAttribute("list",list);
        return "/list";
    }

    /**
     * 文章保存
     */
    @PostMapping("/save")
    public RubbishResult save(@RequestBody ArticleSaveParam param){
        Integer articleId = this.articleService.save(param);
        return RubbishResult.ok(articleId);
    }

    /**
     * 文章详情
     */
    @GetMapping("/detail/{url}")
    public RubbishResult detail(@PathVariable("url") String url){
        return RubbishResult.ok(this.articleService.detail(url));
    }
}
