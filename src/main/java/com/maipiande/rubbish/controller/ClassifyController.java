package com.maipiande.rubbish.controller;

import com.maipiande.rubbish.RubbishResult;
import com.maipiande.rubbish.entity.Classify;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author yizmao
 * @date 18-8-1 下午2:12
 */
@RestController
@RequestMapping("/classify")
public class ClassifyController {

    @GetMapping("/list")
    public RubbishResult list(){
        Classify classify = new Classify();
        List list = classify.selectAll();
        return RubbishResult.ok(list);
    }
}
