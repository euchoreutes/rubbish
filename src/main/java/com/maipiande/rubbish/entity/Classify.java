package com.maipiande.rubbish.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yizmao
 * @date 18-8-1 下午2:13
 */
@Data
public class Classify extends Model {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;

    private String url;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
